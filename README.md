# Game of Life Simulator

Just a small Game of Life simulator written in C++

## Requirements

 - Qt 5 (only the GUI)

All the functionality of the game of life is in the core library,
which has no external dependencies.

The core library has the Eigen3 library bundled for the 2D vector class.

## Build Requirements

 - CMake 3.8+

## Build Instructions

```sh
mkdir build && cd build
cmake ..
```

## Screenshot

![Glider Gun](./.gitlab/screenshot1.png "Glider Gun")
