#include <iostream>

#include <GameOfLife.hpp>

void GameOfLife_Test()
{
    GameOfLife::Grid grid;
    grid.resize(3, 4);
    grid.fill(false);

    grid(0, 0) = false;
    grid(0, 1) = true;
    grid(0, 2) = true;
    grid(0, 3) = false;

    grid(1, 0) = true;
    grid(1, 1) = true;
    grid(1, 2) = false;
    grid(1, 3) = false;

    grid(2, 0) = true;
    grid(2, 1) = false;
    grid(2, 2) = true;
    grid(2, 3) = false;

    std::cout << GameOfLife::countNeighbors(grid, {1, 1}) << std::endl; // 5
    std::cout << GameOfLife::countNeighbors(grid, {1, 3}) << std::endl; // 2
    std::cout << GameOfLife::countNeighbors(grid, {0, 0}) << std::endl; // 3

    std::cout << grid << std::endl << std::endl;

    for (auto i = 0; i < 10; i++)
    {
        GameOfLife::computeNextGeneration(grid);
        std::cout << grid << std::endl << std::endl;;
    }
}

int main(void)
{
    GameOfLife_Test();
}
