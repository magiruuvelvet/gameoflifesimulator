#include "GridWidget.hpp"

#include <QPainter>

#include <QPaintEvent>
#include <QMouseEvent>

GridWidget::GridWidget(QWidget *parent)
    : QWidget(parent)
{
    this->setAutoFillBackground(true);
    this->setGridBackground(_gridBg);
}

GridWidget::~GridWidget()
{
}

void GridWidget::setSpacing(size horizonal, size vertical)
{
    this->_hSpacing = horizonal;
    this->_vSpacing = vertical;
}

void GridWidget::setSpacing(size both)
{
    this->_hSpacing = both;
    this->_vSpacing = both;
}

void GridWidget::setHorizonalSpacing(size spacing)
{
    this->_hSpacing = spacing;
}

void GridWidget::setVerticalSpacing(size spacing)
{
    this->_vSpacing = spacing;
}

const GridWidget::size &GridWidget::horizonalSpacing() const
{
    return _hSpacing;
}

const GridWidget::size &GridWidget::verticalSpacing() const
{
    return _vSpacing;
}

void GridWidget::fillCell(const CellPosition<size> &pos, const QColor &color)
{
    _filledCells.insert(pos, color);
}

void GridWidget::fillCell(size x, size y, const QColor &color)
{
    _filledCells.insert(CellPosition<size>{x, y}, color);
}

void GridWidget::clearCell(const CellPosition<size> &pos)
{
    _filledCells.remove(pos);
}

void GridWidget::clearCell(size x, size y)
{
    _filledCells.remove(CellPosition<size>{x, y});
}

void GridWidget::clearCells()
{
    _filledCells.clear();
}

void GridWidget::setUsedCells(size x, size y)
{
    _useRange = true;
    _xRange = x;
    _yRange = y;
}

void GridWidget::setUsedCells()
{
    _useRange = false;
    _xRange = 0;
    _yRange = 0;
}

void GridWidget::setUnusedCellColor(const QColor &color)
{
    _unusedCellColor = color;
}

void GridWidget::setGridColor(const QColor &color)
{
    _gridColor = color;
}

void GridWidget::setGridBackground(const QColor &color)
{
    _gridBg = color;

    QPalette pal = this->palette();
    pal.setColor(QPalette::All, QPalette::Window, _gridBg);
    this->setPalette(pal);
}

GridWidget::size GridWidget::gridWidth() const
{
    return static_cast<size>(this->width()) / _hSpacing;
}

GridWidget::size GridWidget::gridHeight() const
{
    return static_cast<size>(this->height()) / _vSpacing;
}

void GridWidget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setPen(QPen(_gridColor, 1, Qt::SolidLine));

    // fill entire widget with horizonal lines with the given spacing
    QVector<QLine> horizonalLines;
    for (auto i = 0; i < this->height(); i += _hSpacing)
    {
        horizonalLines.push_back(QLine(0, i, this->width(), i));
    }
    p.drawLines(horizonalLines);

    // fill entire widget with vertical lines with the given spacing
    QVector<QLine> verticalLines;
    for (auto i = 0; i < this->width(); i += _vSpacing)
    {
        verticalLines.push_back(QLine(i, 0, i, this->height()));
    }
    p.drawLines(verticalLines);

    // fill all cells with the given color
    QMapIterator<CellPosition<size>, QColor> cells(_filledCells);
    while (cells.hasNext())
    {
        cells.next();
        p.fillRect(cells.key().x * _hSpacing + 1, cells.key().y * _vSpacing + 1,
                   _hSpacing - 1, _vSpacing - 1, cells.value());
    }

    // deactivate unused cells (if any)
    if (_useRange)
    {
        for (size x = 0U; x < gridWidth() + 1; x++)
        {
            for (size y = 0U; y < gridHeight() + 1; y++)
            {
                if (_xRange <= x || _yRange <= y)
                {
                    p.fillRect(x * _hSpacing + 1, y * _vSpacing + 1,
                               _hSpacing - 1, _vSpacing -1, _unusedCellColor);
                }
            }
        }
    }

    event->accept();
}

void GridWidget::mousePressEvent(QMouseEvent *event)
{
    emit cellClicked(CellPosition<size>{
        static_cast<size>(event->pos().x()) / _hSpacing,
        static_cast<size>(event->pos().y()) / _vSpacing});
}
