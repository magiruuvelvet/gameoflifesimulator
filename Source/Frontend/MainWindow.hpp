#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <memory>

#include <QWidget>
#include <QBoxLayout>
#include <QPushButton>
#include <QComboBox>

#include <QTimer>

#include "GridWidget.hpp"

#include <GameOfLife.hpp>

class MainWindow : public QWidget
{
public:
    explicit MainWindow(QWidget *parent = nullptr);

    /**
     * Fills all active cells in the grid widget.
     */
    void fillCells(const GameOfLife::Grid &cells);

    /**
     * Clears all cells in the grid widget.
     */
    void clearCells();

    /**
     * Helper function to compute the next generation of game of life.
     */
    void computeNextGeneration();

    /**
     * Start the timer and automatically compute generations.
     */
    void startProcessing();

    /**
     * Stop the timer and stop computing generations. All cells are preserved.
     */
    void stopProcessing();

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void closeEvent(QCloseEvent *event);

private:
    std::shared_ptr<QVBoxLayout> vbox;
    std::shared_ptr<QHBoxLayout> buttonBox;

    std::shared_ptr<GridWidget> grid;

    std::shared_ptr<QTimer> timer;

    std::shared_ptr<QPushButton> btnStartStop;
    std::shared_ptr<QComboBox> cbPattern;
    std::shared_ptr<QPushButton> btnClear;

    static const QString txtStart;
    static const QString txtStop;
    static const QString txtClear;

    GameOfLife::Grid cells;
};

#endif // MAINWINDOW_HPP
