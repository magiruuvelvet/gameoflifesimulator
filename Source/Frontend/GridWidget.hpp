#ifndef GRIDWIDGET_HPP
#define GRIDWIDGET_HPP

#include <QMap>

#include <QWidget>

#include <GameOfLifeTypes.hpp>

class GridWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * Size type (precision) of the grid and its coordinates.
     */
    using size = CellPosition<>::precision;

    explicit GridWidget(QWidget *parent = nullptr);
    ~GridWidget();

    /**
     * Sets the line spacing for horizonal and vertical lines.
     */
    void setSpacing(size horizonal, size vertical);
    void setSpacing(size both);
    void setHorizonalSpacing(size spacing);
    void setVerticalSpacing(size spacing);

    /**
     * Returns the current line spacing.
     */
    const size &horizonalSpacing() const;
    const size &verticalSpacing() const;

    /**
     * Fill the cell at the given position with a specific color.
     */
    void fillCell(const CellPosition<size> &pos, const QColor &color = Qt::black);
    void fillCell(size x, size y, const QColor &color = Qt::black);

    /**
     * Clear the fill color of the cell at the given position.
     */
    void clearCell(const CellPosition<size> &pos);
    void clearCell(size x, size y);

    /**
     * Clears all cells in the grid.
     */
    void clearCells();

    /**
     * Sets a range of used cells, all other cells will be grayed out.
     */
    void setUsedCells(size x, size y);

    /**
     * Clears the used cells range.
     */
    void setUsedCells();

    /**
     * Sets the color used for unused cells.
     */
    void setUnusedCellColor(const QColor &color);

    /**
     * Sets the color of the grid lines.
     */
    void setGridColor(const QColor &color);

    /**
     * Sets the background color of the grid.
     */
    void setGridBackground(const QColor &color);

    /**
     * Number of full horizonal cells. Partly cut cells are ignored.
     */
    size gridWidth() const;

    /**
     * Number of full vertical cells. Partly cut cells are ignored.
     */
    size gridHeight() const;

signals:
    void cellClicked(const CellPosition<size> &pos);

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    size _hSpacing = 15;
    size _vSpacing = 15;

    bool _useRange = false;
    size _xRange = 0;
    size _yRange = 0;

    QColor _unusedCellColor = Qt::lightGray;

    QColor _gridColor = Qt::black;
    QColor _gridBg = Qt::white;
    QMap<CellPosition<size>, QColor> _filledCells;
};

// QHash support for CellPosition
inline uint qHash(const CellPosition<GridWidget::size> &key)
{
    return qHash(QPair<GridWidget::size, GridWidget::size>(key.x, key.y));
}

// QMap support for CellPosition, use qHash for value calculation
inline bool operator< (const CellPosition<GridWidget::size> &left, const CellPosition<GridWidget::size> &right)
{
    return qHash(left) < qHash(right);
}

#endif // GRIDWIDGET_HPP
