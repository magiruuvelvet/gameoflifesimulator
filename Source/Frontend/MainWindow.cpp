#include "MainWindow.hpp"

#include <iostream>

#include <QPaintEvent>
#include <QResizeEvent>
#include <QCloseEvent>

#include <GameOfLifePatterns.hpp>

const QString MainWindow::txtStart = "Start";
const QString MainWindow::txtStop = "Stop";
const QString MainWindow::txtClear = "Clear";

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    this->resize(600, 600);

    // Setup Timer
    timer = std::make_shared<QTimer>();
    timer->setInterval(1000); // 1s
    QObject::connect(timer.get(), &QTimer::timeout, this, [&]{
        timer->setInterval(1000);
        computeNextGeneration();
        timer->start();
    });

    // Setup Layout
    vbox = std::make_shared<QVBoxLayout>();
    vbox->setSpacing(0);
    vbox->setMargin(0);
    vbox->setContentsMargins(0, 0, 0, 0);

    // Create Grid
    grid = std::make_shared<GridWidget>();
    grid->setGridColor(Qt::gray);
    grid->setGridBackground(Qt::white);
    grid->setUnusedCellColor(QColor("#ececec"));
    grid->setSpacing(13);

    vbox->addWidget(grid.get());

    // Setup Button Box
    buttonBox = std::make_shared<QHBoxLayout>();
    buttonBox->setSpacing(3);
    buttonBox->setMargin(0);
    buttonBox->setContentsMargins(3, 3, 3, 3);

    vbox->addLayout(buttonBox.get());

    // Start/Stop Button
    btnStartStop = std::make_shared<QPushButton>(txtStart);
    QObject::connect(btnStartStop.get(), &QPushButton::clicked, this, [&]{
        if (timer->isActive())
        {
            stopProcessing();
        }
        else
        {
            startProcessing();
        }
    });
    buttonBox->addWidget(btnStartStop.get());

    // Pattern Selector
    cbPattern = std::make_shared<QComboBox>();
    cbPattern->setToolTip("Select a pattern");
    cbPattern->addItems({
        "",           // 0
        "Glider",     // 1
        "Glider Gun", // 2
        "Random",     // 3

        "Debug: loadPattern"
    });
    QObject::connect(cbPattern.get(), static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [&](int index){
        switch (index)
        {
            case 0:
                stopProcessing();
                clearCells();
                break;

            // Glider
            case 1:
                cells = GameOfLifePatterns::Glider();
                GameOfLife::resizeGrid(cells, grid->gridWidth() + 1, grid->gridHeight() + 1);
                fillCells(cells);
                break;

            // Glider Gun
            case 2:
                cells = GameOfLife::applyOffset(GameOfLifePatterns::GliderGun(), 10, 10);
                GameOfLife::resizeGrid(cells, grid->gridWidth() + 1, grid->gridHeight() + 1);
                fillCells(cells);
                break;

            // Random
            case 3:
                cells = GameOfLife::generateRandomGrid(grid->gridWidth() + 1, grid->gridHeight() + 1);
                fillCells(cells);
                break;

            // Debug: loadPattern
            case 4:
                clearCells();
                for (auto i = 0; i < 5; i++)
                {
                    GameOfLife::loadPattern(cells, GameOfLifePatterns::Glider(), i * 5, i * 5);
                }
                fillCells(cells);
                break;
        }
    });
    cbPattern->setCurrentIndex(0);
    buttonBox->addWidget(cbPattern.get());

    // Clear Button
    btnClear = std::make_shared<QPushButton>(txtClear);
    QObject::connect(btnClear.get(), &QPushButton::clicked, this, [&]{
        stopProcessing();
        clearCells();
        cbPattern->setCurrentIndex(0);
    });
    buttonBox->addWidget(btnClear.get());

    this->setLayout(vbox.get());

    // Callback when a cell was clicked
    QObject::connect(grid.get(), &GridWidget::cellClicked, this, [&](const CellPosition<GridWidget::size> &pos){
        if (cells(pos.x, pos.y))
        {
            cells(pos.x, pos.y) = false;
        }
        else
        {
            cells(pos.x, pos.y) = true;
        }
        fillCells(cells);
    });

    // Make sure all cells are initialy cleared
    clearCells();
}

void MainWindow::fillCells(const GameOfLife::Grid &cells)
{
    grid->clearCells();

    // nothing to do
    if (cells.size() == 0)
    {
        return;
    }

    // fill active cells in the grid widget
    for (auto i = 0U; i < cells.rows(); i++)
    {
        for (auto j = 0U; j < cells.cols(); j++)
        {
            if (cells(i, j))
            {
                grid->fillCell(i, j);
            }
        }
    }

    grid->update();
}

void MainWindow::clearCells()
{
    cells.resize(grid->gridWidth() + 1, grid->gridHeight() + 1);
    cells.setZero();
    fillCells(cells);
}

void MainWindow::computeNextGeneration()
{
    GameOfLife::computeNextGeneration(cells);
    fillCells(cells);
}

void MainWindow::startProcessing()
{
    btnStartStop->setText(txtStop);
    timer->start();
}

void MainWindow::stopProcessing()
{
    btnStartStop->setText(txtStart);
    timer->stop();
    timer->setInterval(1000);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    event->accept();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    if (cells.rows() == 0 && cells.cols() == 0)
    {
        return;
    }

    GameOfLife::resizeGrid(cells, grid->gridWidth() + 1, grid->gridHeight() + 1);

    fillCells(cells);
    event->accept();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    stopProcessing();
    event->accept();
}
