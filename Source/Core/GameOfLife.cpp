#include "GameOfLife.hpp"

#include "Internal/Randomizer.hpp"

/**
 * • If a dead cell has three living neighbors, it becomes alive in the next generation.
 * • If a live cell has two or three living neighbors, it lives on in the next generation.
 * • If a live cell has fewer than two living neighbors, it dies in the next generation.
 * • If a live cell has more than three neighbors, it dies in the next generation.
 */

void GameOfLife::computeNextGeneration(Grid &grid)
{
    const auto current = grid;

    // birth: 3 neighbors
    for (auto i = 0U; i < current.rows(); i++)
    {
        for (auto j = 0U; j < current.cols(); j++)
        {
            // ignore alive cells in this step
            if (current(i, j))
            {
                continue;
            }

            const auto neighbors = countNeighbors(current, {i, j});
            if (neighbors == 3U)
            {
                grid(i, j) = true;
            }
        }
    }

    // life: 2,3 neighbors
    for (auto i = 0U; i < current.rows(); i++)
    {
        for (auto j = 0U; j < current.cols(); j++)
        {
            // ignore dead cells in this step
            if (!current(i, j))
            {
                continue;
            }

            const auto neighbors = countNeighbors(current, {i, j});
            if (neighbors == 2 || neighbors == 3)
            {
                grid(i, j) = true;
            }
            else
            {
                grid(i, j) = false;
            }
        }
    }
}

unsigned int GameOfLife::countNeighbors(const Grid &grid, const CellPosition<> &pos)
{
    /**
     *  [x-1  [x-1] [x-1
     *   y-1]  y]    y+1]
     *
     *  [x    [x    [x
     *   y-1]  y]    y+1]
     *
     *  [x+1  [x+1  [x+1
     *   y-1]  y     y+1]
     */

    auto count = 0U;

    // ignore counting when grid is zero or the cell position is larger than the grid itself
    if (grid.size() == 0 ||
        pos.x >= static_cast<unsigned long long>(grid.rows()) ||
        pos.y >= static_cast<unsigned long long>(grid.cols()))
    {
        return count;
    }

    const auto count_helper = [&grid](const unsigned long long &x, const unsigned long long &y) -> unsigned int {

        if (x >= static_cast<unsigned long long>(grid.rows()) ||
            y >= static_cast<unsigned long long>(grid.cols()))
        {
            return 0U;
        }

        return grid(Grid::Index(x), Grid::Index(y)) ? 1U : 0U;
    };

    count += count_helper(pos.x-1, pos.y-1);
    count += count_helper(pos.x-1, pos.y);
    count += count_helper(pos.x-1, pos.y+1);

    count += count_helper(pos.x, pos.y-1);
    //-- don't count self
    count += count_helper(pos.x, pos.y+1);

    count += count_helper(pos.x+1, pos.y-1);
    count += count_helper(pos.x+1, pos.y);
    count += count_helper(pos.x+1, pos.y+1);

    return count;
}

const GameOfLife::Grid GameOfLife::generateRandomGrid(const Grid::Index &rows, const Grid::Index &cols)
{
    Grid grid(rows, cols);

    for (auto x = 0U; x < grid.rows(); x++)
    {
        for (auto y = 0U; y < grid.cols(); y++)
        {
            grid(x, y) = Randomizer::GetRandomUInt<bool>(0, 1);
        }
    }

    return grid;
}

void GameOfLife::resizeGrid(Grid &grid, const Grid::Index &rows, const Grid::Index &cols)
{
    const auto backup = grid;

    // resize grid (losing state)
    grid.resize(rows, cols);
    grid.setZero();

    // restore state
    for (auto x = 0U; x < backup.rows(); x++)
    {
        for (auto y = 0U; y < backup.cols(); y++)
        {
            if (!(x >= grid.rows() || y >= grid.cols()))
            {
                grid(x, y) = backup(x, y);
            }
        }
    }
}

const GameOfLife::Grid GameOfLife::applyOffset(const Grid &grid, const Grid::Index &offx, const Grid::Index &offy)
{
    // return given grid if offset is zero
    if (offx == 0 && offy == 0)
    {
        return grid;
    }

    // first create a new grid of bigger size (original size + offset)
    Grid new_grid;
    new_grid.resize(grid.rows() + offx, grid.cols() + offy);
    new_grid.setZero();

    // transfer the cells to the new grid
    for (auto x = offx; x < new_grid.rows(); x++)
    {
        for (auto y = offy; y < new_grid.cols(); y++)
        {
            new_grid(x, y) = grid(x - offx, y - offy);
        }
    }

    // return the new grid
    return new_grid;
}

void GameOfLife::loadPattern(Grid &grid, const Grid &pattern, const Grid::Index &offx, const Grid::Index &offy)
{
    // check if resize is needed (growing)
    const auto max_size = gridSize(pattern) + GridSize{offx, offy};
    if (gridSize(grid) < max_size)
    {
        resizeGrid(grid, max_size.width, max_size.height);
    }

    // offset the pattern
    const auto offset_pattern = applyOffset(pattern, offx, offy);

    // apply and overwrite cells
    for (auto x = offx; x < offset_pattern.rows(); x++)
    {
        for (auto y = offy; y < offset_pattern.cols(); y++)
        {
            grid(x, y) = offset_pattern(x, y);
        }
    }
}
