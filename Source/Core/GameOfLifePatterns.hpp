#ifndef GAMEOFLIFEPATTERNS_HPP
#define GAMEOFLIFEPATTERNS_HPP

#include "GameOfLife.hpp"

class GameOfLifePatterns
{
    GameOfLifePatterns() = delete;

public:
    static const GameOfLife::Grid Glider();
    static const GameOfLife::Grid GliderGun();
};

#endif // GAMEOFLIFEPATTERNS_HPP
