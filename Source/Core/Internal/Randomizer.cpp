#include "Randomizer.hpp"

#include <algorithm>
#include <fstream>

#include <sys/stat.h>
#include <unistd.h>

std::vector<std::uint64_t> Randomizer::history;
const std::uint64_t Randomizer::max_attempts = 50;

void Randomizer::SetHistorySize(std::uint64_t size)
{
    history.clear();
    history.resize(size);
    std::fill(history.begin(), history.end(), 0);
}

std::uint64_t Randomizer::GetRandomNumber()
{
    auto now = QueryRandomDevice();

    if (history.size() != 0)
    {
        std::uint64_t current_attempt = 0;
        while (std::find(history.begin(), history.end(), now) != history.end())
        {
            if (current_attempt > max_attempts)
            {
                break;
            }

            now = QueryRandomDevice();
            current_attempt++;
        }

        history.erase(history.begin());
        history.push_back(now);

        //LOG_ERROR("size=%i, now=%llu, first=%llu, prev=%llu", history.size(), now, history.first(), history.last());
    }

    return now;
}

std::uint64_t Randomizer::GetRandomNumber(std::uint64_t min, std::uint64_t max)
{
    if (min == 0ULL && max == 0ULL)
    {
        return 0;
    }

    if (min > max)
    {
        return 0;
    }

    auto now = _GetRangedRandomNumberHelper(min, max, QueryRandomDevice());

    // only use history if enabled (size > 0) AND max is greater or equal to the history size
    // the second size check is required to avoid stuck (never changing) results
    if (history.size() != 0 && max >= static_cast<std::uint64_t>(history.size()))
    {
        std::uint64_t current_attempt = 0;
        while (std::find(history.begin(), history.end(), now) != history.end())
        {
            if (current_attempt > max_attempts)
            {
                break;
            }

            now = _GetRangedRandomNumberHelper(min, max, QueryRandomDevice());
            current_attempt++;
        }

        history.erase(history.begin());
        history.push_back(now);

        //LOG_ERROR("size=%i, now=%llu, first=%llu, prev=%llu", history.size(), now, history.first(), history.last());
    }

    return now;
}

std::uint64_t Randomizer::QueryRandomDevice(bool *error)
{
    static const auto ProbeRandomDevice = [](const char *random_device) -> bool {
        struct stat buffer;
        return (stat(random_device, &buffer) == 0 && buffer.st_mode & S_IRUSR);
    };

    if (ProbeRandomDevice("/dev/urandom"))
    {
        return QueryRandomDevice_Helper("/dev/urandom", error);
    }
    else if (ProbeRandomDevice("/dev/random"))
    {
        return QueryRandomDevice_Helper("/dev/random", error);
    }
    else
    {
        if (error)
        {
            *error = true;
        }
    }
    return 0;
}

std::uint64_t Randomizer::QueryRandomDevice_Helper(const char *random_device, bool *error)
{
    std::uint64_t rand_val = 0;
    std::streamsize size = sizeof(rand_val);

    std::ifstream rand_device(random_device, std::ios::in | std::ios::binary);
    if (rand_device)
    {
        rand_device.read(reinterpret_cast<char*>(&rand_val), size);
        if (rand_device)
        {
            if (error)
            {
                *error = false;
            }
        }
        else
        {
            if (error)
            {
                *error = true;
            }
        }
        rand_device.close();
    }
    else
    {
        if (error)
        {
            *error = true;
        }
    }

    return rand_val;
}

std::uint64_t Randomizer::_GetRangedRandomNumberHelper(std::uint64_t min, std::uint64_t max, std::uint64_t seed)
{
    // min~max: res = min + ($random_number % static_cast<T>(max - min + 1))
    return min + (seed % static_cast<std::uint64_t>(max - min + 1));
}
