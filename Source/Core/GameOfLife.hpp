#ifndef GAMEOFLIFE_HPP
#define GAMEOFLIFE_HPP

#include <Eigen/Core>

#include "GameOfLifeTypes.hpp"

class GameOfLife
{
    GameOfLife() = delete;

public:
    using Grid = Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic>;
    using GridSize = GridSize<Grid::Index>;

    /**
     * Constructs a GridSize struct with the size of the grid.
     */
    static inline const GridSize gridSize(const Grid &grid)
    {
        return { grid.rows(), grid.cols() };
    }

    /**
     * Computes the next generation of the grid.
     * Grid is modified in place.
     */
    static void computeNextGeneration(Grid &grid);

    /**
     * Counts all alive neighbors in the grid.
     */
    static unsigned int countNeighbors(const Grid &grid, const CellPosition<> &pos);

    /**
     * Generates a random grid of the given size.
     */
    static const Grid generateRandomGrid(const Grid::Index &rows, const Grid::Index &cols);

    /**
     * Resizes the current grid while preserving its current content.
     * Cells which don't fit into the new size are lost.
     * New cells are dead by default.
     */
    static void resizeGrid(Grid &grid, const Grid::Index &rows, const Grid::Index &cols);

    /**
     * Applies an offset to the given grid, resizing it on the top and left side.
     */
    static const Grid applyOffset(const Grid &grid, const Grid::Index &offx, const Grid::Index &offy);

    /**
     * Loads the given pattern into the existing grid, resizing it if required.
     * Optimally the pattern can be placed at the given offset. (default is 0x0)
     */
    static void loadPattern(Grid &grid, const Grid &pattern, const Grid::Index &offx = 0U, const Grid::Index &offy = 0U);
};

#endif // GAMEOFLIFE_HPP
