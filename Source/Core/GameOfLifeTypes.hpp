#ifndef GAMEOFLIFETYPES_HPP
#define GAMEOFLIFETYPES_HPP

#include <type_traits>

/**
 * CellPosition template struct
 *
 * Struct to store coordinates.
 *
 * Only integral types are supported.
 *
 */
template<typename _Precision = unsigned long long, typename = std::enable_if<std::is_integral<_Precision>::value>>
struct CellPosition
{
    using precision = _Precision;
    precision x = 0;
    precision y = 0;
};

/**
 * GridSize template struct
 *
 * Struct to store and manage a grid size easily.
 *
 * Only integral types are supported.
 *
 */
template<typename _Precision, typename = std::enable_if<std::is_integral<_Precision>::value>>
struct GridSize
{
    using precision = _Precision;

    precision width = 0;
    precision height = 0;

    /**
     * addition operator
     */
    template<typename T>
    const GridSize<precision> operator+ (const GridSize<T> &other) const
    {
        return { width + other.width, height + other.height };
    }

    /**
     * subtraction operator
     */
    template<typename T>
    const GridSize<precision> operator- (const GridSize<T> &other) const
    {
        return { width - other.width, height - other.height };
    }

    /**
     * lesser than operator
     */
    template<typename T>
    bool operator< (const GridSize<T> &other) const
    {
        return width < other.width && height < other.height;
    }

    /**
     * lesser than or equal operator
     */
    template<typename T>
    bool operator<= (const GridSize<T> &other) const
    {
        return width <= other.width && height <= other.height;
    }

    /**
     * greater than operator
     */
    template<typename T>
    bool operator> (const GridSize<T> &other) const
    {
        return width > other.width && height > other.height;
    }

    /**
     * greater than or equal operator
     */
    template<typename T>
    bool operator>= (const GridSize<T> &other) const
    {
        return width >= other.width && height >= other.height;
    }
};

#endif // GAMEOFLIFETYPES_HPP
